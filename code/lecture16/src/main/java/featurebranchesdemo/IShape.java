package featurebranchesdemo;

/**
 * Interface to represent all Shape objects
 * 
 * @author Swetha Rajagopal
 * @version 10/30/2023
 * 
 */
public interface IShape {
    double getArea();
}
